import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { VacantesComponent } from './components/vacantes/vacantes.component';
import { AddVacantesComponent } from './components/add-vacantes/add-vacantes.component';
import { EditVacantesComponent } from './components/edit-vacantes/edit-vacantes.component';
import { ErrorComponent } from './components/error/error.component';
import { MenuComponent } from './components/menu/menu.component';
import { ModulosComponent } from './components/modulos/modulos.component';


const appRoutes : Routes = [
	{path: '', component: InicioComponent},
	{path: 'vacantes', component: VacantesComponent},
	{path: 'agregar-vacantes', component: AddVacantesComponent},
	{path: 'editar-vacantes/:id/:nombre/:idSubarea/:idPuesto/:idTipoPuesto/:idTipo', component: EditVacantesComponent},
	{path: 'menu', component: MenuComponent},
	{path: 'modulos', component: ModulosComponent},
	{path: 'error', component: ErrorComponent},
	{path: '**', component: ErrorComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);


			
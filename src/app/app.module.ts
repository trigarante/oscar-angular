//Componentes
import { InicioComponent } from './components/inicio/inicio.component';
import { VacantesComponent } from './components/vacantes/vacantes.component';
import { AddVacantesComponent } from './components/add-vacantes/add-vacantes.component';
import { EditVacantesComponent } from './components/edit-vacantes/edit-vacantes.component';
import { MenuComponent } from './components/menu/menu.component';
import { ModulosComponent } from './components/modulos/modulos.component';
import { AppComponent } from './app.component';

//Dependencias
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api';                 //api
import { PanelModule } from 'primeng/components/panel/panel';
import { ButtonModule } from 'primeng/components/button/button';
import { RadioButtonModule } from 'primeng/components/radioButton/radioButton';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import {PanelMenuModule} from 'primeng/panelmenu';
import { HttpClientModule} from '@angular/common/http';
import { routing, appRoutingProviders } from './app.routing';
import { AppRoutingModule } from './app-routing.module';
import { ErrorComponent } from './components/error/error.component';
import {DropdownModule} from 'primeng/dropdown';
import {TabMenuModule} from 'primeng/tabmenu';
import {TableModule} from 'primeng/table';
import {Component} from '@angular/core';
import {MessageService} from 'primeng/api';
import {PaginatorModule} from 'primeng/paginator';
import {DataViewModule} from 'primeng/dataview';
import {CardModule} from 'primeng/card';
import {DialogModule} from 'primeng/dialog';
import {MenuModule} from 'primeng/menu';
import {MessagesModule} from 'primeng/messages';
import {MessageModule} from 'primeng/message';
import {Message} from 'primeng//api';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalDialogModule } from 'ngx-modal-dialog';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ModalModule, BsModalService} from 'ngx-bootstrap/modal';
import {ToastModule} from 'primeng/toast';







@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    InicioComponent,
    VacantesComponent,
    AddVacantesComponent,
    EditVacantesComponent,
    MenuComponent,
    ModulosComponent,

   

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AccordionModule,
    PanelModule,
    RadioButtonModule,
    AppRoutingModule,
    routing,
    HttpClientModule,
    DropdownModule,
    TabMenuModule,
    TableModule,
    ButtonModule,
    PaginatorModule,
    DataViewModule,
    CardModule,
    DialogModule,
    MenuModule,
    PanelMenuModule,
    MessagesModule,
    MessageModule,
    NgbModule,
    ModalDialogModule.forRoot(),
    ModalModule.forRoot(),
    ToastModule

    
    

  ],
  providers: [
    	appRoutingProviders,
        BsModalService],
  bootstrap: [AppComponent],
  entryComponents: [ModulosComponent]
})
export class AppModule { }


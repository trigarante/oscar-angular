import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVacantesComponent } from './add-vacantes.component';

describe('AddVacantesComponent', () => {
  let component: AddVacantesComponent;
  let fixture: ComponentFixture<AddVacantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddVacantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVacantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Vacante } from '../../models/vacante';
import { VacanteService } from '../../services/vacante.service';
import { Global } from  '../../services/global';
import {SelectItem} from 'primeng/components/common/api';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { Router } from '@angular/router';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-vacantes',
  templateUrl: '../add-vacantes/add-vacantes.component.html',
  styleUrls: ['./add-vacantes.component.css'],
  providers: [VacanteService]
})
export class AddVacantesComponent implements OnInit {
	public title: string;
	public vacante: Vacante;
	public url: string;	
  constructor( 
  		private _vacanteService: VacanteService,
      private router: Router,
      config: NgbModalConfig, private modalService: NgbModal
  		) { 
  			this.title = "Crear Vacante";
  			this.vacante = new Vacante(0,'',0,0,0,0);
  			this.url = Global.url;
        config.backdrop = 'static';
        config.keyboard = false;
  }

  ngOnInit() {
    this.vacante.id= null;
    this.vacante.nombre=null;
    this.vacante.idPuesto = null;
    this.vacante.idSubarea =null;
    this.vacante.idTipoPuesto = null;
    this.vacante.idTipo = null;
    
  }
  open(content) {
    this.modalService.open(content);
   
  }
  onSubmit(form){
  	console.log(this.vacante);
  	this._vacanteService.saveVacante(this.vacante).subscribe(
        response => {
          console.log(response);
          form.reset();
          this.router.navigate(['/vacantes']);

        },
        error=>{
          console.log(<any>error);
        }
      );
  }

}

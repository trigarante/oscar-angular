import { Component, OnInit } from '@angular/core';
import {PanelMenuModule} from 'primeng/panelmenu';
import {MenuItem} from 'primeng/api';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
    items: MenuItem[];


    ngOnInit() {
        this.items = [
            {
                label: 'Solitudes',
                icon: 'pi pi-pw pi-bars',
                items: [
                    {label: 'Registro', icon: 'pi pi-fw pi-user-plus'},
                    {separator: true},
                    {label: 'Reporte', icon: 'pi pi-fw pi-filter'}
                ]
            },
            {
                label: 'Precandidatos',
                icon: 'pi pi-pw pi-bars',
                items: [
                    {label: 'Registro', icon: 'pi pi-fw pi-user-plus'},
                    {separator: true},
                    {label: 'Reporte', icon: 'pi pi-fw pi-filter'}
                ]
            },
            {
                label: 'Capacitación',
                icon: 'pi pi-pw pi-bars',
                items: [
                    {label: 'Registro', icon: 'pi pi-fw pi-user-plus'},
                    {separator: true},
                    {label: 'Reporte', icon: 'pi pi-fw pi-filter'}
                ]
            },
            {
                label: 'Seguimiento',
                icon: 'pi pi-pw pi-bars',
                items: [
                    {label: 'Registro', icon: 'pi pi-fw pi-user-plus'},
                    {separator: true},
                    {label: 'Reporte', icon: 'pi pi-fw pi-filter'}
                ]
            },
            {
                label: 'Empleados',
                icon: 'pi pi-fw pi-user',
                items: [
                    {label: 'Empleado', icon: 'pi pi-fw pi-search'},
                    {label: 'Alta Imss', icon: 'pi pi-fw pi-plus'}
                ]
            },
            {
                label: 'Catalogos',
                icon: 'pi pi-fw pi-file',
                items: [
                    {
                        label: 'Vacantes',
                        icon: 'pi pi-pi pi-tag',
                        items: [
		                    {label: 'Mostrar', icon: 'pi pi-fw pi-search',routerLink:'/vacantes'},
		                    {label: 'Agregar', icon: 'pi pi-fw pi-plus',routerLink:'/agregar-vacantes'}
		                ]
                    },
                    
                ]
            },
            {
                label: 'Reportes',
                icon: 'pi pi-fw pi-file',
                items: [
                    {label: 'Precandidato',icon: 'pi pi-pi pi-search'},
                    {label: 'Candidato',icon: 'pi pi-pi pi-search'},
                    {label: 'Antiguedad de empleado',icon: 'pi pi-pi pi-search'},
                    {label: 'Demografico empleado',icon: 'pi pi-pi pi-search'},
                    {label: 'Seguimiento',icon: 'pi pi-pi pi-search'},

                    
                ]
            },
            {
                label: 'Modulos',
                icon: 'pi pi-fw pi-file',
                routerLink:'/modulos'
            }
        ];
    }

}

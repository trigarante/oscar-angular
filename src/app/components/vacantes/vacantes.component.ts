import { Component, OnInit} from '@angular/core';
import { Vacante } from '../../models/vacante';
import { VacanteService } from '../../services/vacante.service';
import { Global } from '../../services/global';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {filter, map} from 'rxjs/operators';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {SelectItem} from 'primeng/components/common/api';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {BsModalService} from 'ngx-bootstrap/modal';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ModulosComponent} from '../modulos/modulos.component';




@Component({
  selector: 'app-vacantes',
  templateUrl: './vacantes.component.html',
  styleUrls: ['./vacantes.component.css'],
  providers:[MessageService,VacanteService,NgbActiveModal],
  
  styles: [`
    .dark-modal .modal-content {
      background-color: #292b2c;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
    .light-blue-backdrop {
      background-color: #5cb3fd;
    }
  `]
})
export class VacantesComponent implements OnInit {
	vacantes: Vacante[];
  editVacante: Vacante;
  cols:any[];
  nombre:any[];
  closeResult: string;
  public title: string;
  mensaje: string;

  constructor(
  	private _vacanteService : VacanteService,
    private modalService: NgbModal,
    private _router: Router,
    private _route: ActivatedRoute,
    private bsModalService: BsModalService,
    public activeModal: NgbActiveModal,
    private route: ActivatedRoute,
    private messageService: MessageService

  	) { }
  
  

  ngOnInit() {
     this.route.queryParams.pipe(filter(params => params.mensaje))
      .subscribe(params => {
        console.log(params);
        this.mensaje = params.mensaje;
        if (this.mensaje === 'exito') {
          this.messageService.add({severity: 'success', summary: 'Usuario Creado'});
        }
        console.log(this.mensaje); // popular
      });
    this.editVacante = new Vacante(0,'',0,0,0,0);
    this.title="Catalogos";
    this.getVacantes();
    this.cols = [
            { field: 'id', header: 'id' },
            { field: 'nombre', header: 'nombre' },
            { field: 'idSubarea', header: 'idSubarea' },
            { field: 'idPuesto', header: 'idPuesto' },
            { field: 'idTipoPuesto', header: 'idTipoPuesto' },
            { field: 'idTipo', header: 'idTipo' }
                   ];
    
  }

  getVacantes(){
  	this._vacanteService.getVacantes().subscribe(
  		response => {
  			if(response){
  				this.vacantes = response;
  			}
  		},
  		error => {
  			console.log(<any>error);
  		}
  		)
  }
  

  openLg(content, id,nombre,idSubarea,idPuesto,idTipoPuesto,idTipo) {

    this.editVacante.id = id;
    this.editVacante.nombre = nombre ; 
    this.editVacante.idSubarea = idSubarea;
    this.editVacante.idPuesto = idPuesto;
    this.editVacante.idTipoPuesto = idTipoPuesto;
    this.editVacante.idTipo = idTipo;
    this.modalService.open(content, { size: 'lg' });
  }
  onSubmit(form){
    

    this._vacanteService.saveVacante(this.editVacante).subscribe(
        response => {
          
          form.reset();
          this.getVacantes();

          this.modalService.dismissAll();
          
          
        },
        error=>{
          console.log(<any>error);
        }
      );
  }

  terminar() {
      
    this._router.navigate(['/vacantes']);
  }
  
  openModal(id) {

// content, id,nombre,idSubarea,idPuesto,idTipoPuesto,idTipo
    
    const modal = this.modalService.open(ModulosComponent);  
    modal.componentInstance.id = id;
    //console.log(modal.componentInstance.id);
  //  modal.componentInstance.nombre = nombre;
  
  }
    


}

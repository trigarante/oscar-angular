import { Component, OnInit } from '@angular/core';
import { Vacante } from '../../models/vacante';
import { VacanteService } from '../../services/vacante.service';
import { Global } from  '../../services/global';
import {SelectItem} from 'primeng/components/common/api';
import {Message} from 'primeng/components/common/api';
import {MessageService} from 'primeng/components/common/messageservice';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-edit-vacantes',
  templateUrl: './edit-vacantes.component.html',
  styleUrls: ['./edit-vacantes.component.css'],
  providers: [VacanteService]
})
export class EditVacantesComponent implements OnInit {
	public title: string;
	public vacante: Vacante;
	public url: string;	
	public id : number;
	public nombre : string;
	public idSubarea : number;
	public idPuesto : number;
	public idTipoPuesto : number;
	public idTipo : number;
	public parametros;


  constructor( 
  		private _vacanteService: VacanteService,
  		private _router: Router,
  		private _route: ActivatedRoute
  		) { 
  			this.title = "Editar Vacante";
  			this.vacante = new Vacante(0,'',0,0,0,0);
  			this.url = Global.url;
  }

  ngOnInit() {
  	this._route.params.subscribe(params => {
  			 // 'editar-vacantes/:id/:nombre/:idSubarea/:idPuesto/:idTipoPuesto/:idTipo'
  		 this.vacante.id = params.id;
  		 this.vacante.nombre = params.nombre;
  		 this.vacante.idSubarea = params.idSubarea;
  		 this.vacante.idPuesto = params.idPuesto;
  		 this.vacante.idTipoPuesto = params.idTipoPuesto;
  		 this.vacante.idTipo = params.idTipo;

  	});
  }
  onSubmit(form){
  	console.log(this.vacante);
  	this._vacanteService.saveVacante(this.vacante).subscribe(
        response => {
          console.log(response);
          form.reset();
          
          this._router.navigate(['/vacantes']);
        },
        error=>{
          console.log(<any>error);
        }
      );
  }

}

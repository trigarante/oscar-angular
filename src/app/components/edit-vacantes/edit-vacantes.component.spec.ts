import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditVacantesComponent } from './edit-vacantes.component';

describe('EditVacantesComponent', () => {
  let component: EditVacantesComponent;
  let fixture: ComponentFixture<EditVacantesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditVacantesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditVacantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

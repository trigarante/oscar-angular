import { Component,  Input, OnInit, Output } from '@angular/core';
import { NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Router,ActivatedRoute, Params } from '@angular/router';
import {MessageService} from 'primeng/api';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VacanteService} from '../../services/vacante.service';
import {Global} from '../../services/global';
import { Vacante } from '../../models/vacante';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {filter, map} from 'rxjs/operators';
import {SelectItem} from 'primeng/components/common/api';
import {Message} from 'primeng/components/common/api';
import {BsModalService} from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-modulos',
  templateUrl: './modulos.component.html',
  styleUrls: ['./modulos.component.css'],
   providers: [MessageService],
   
})
	export class ModulosComponent {
  @Input() idCatalogo= 0;
  
  vacanteForm: FormGroup;
  editVacante: Vacante;
  userParameter = 0;
  submitted = false;

	public title: string;
  constructor(public activeModal: NgbActiveModal, 
              private router: Router,
              private messageService: MessageService,
              private formBuilder: FormBuilder) {
  }
   
  

     ngOnInit() {
 
    this.title = "Cochis";
    this.vacanteForm = this.formBuilder.group({
     id_Catalogo: [this.idCatalogo, Validators.required]
    });
   console.log(this.vacanteForm);

    
   
  }
   get f() {
    return this.vacanteForm.controls;
  }
  

  

}

  




// this.activeModal.close();



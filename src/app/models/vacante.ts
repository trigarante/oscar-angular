export class Vacante {
	constructor(
		public id: number,
		public nombre: string,
		public idSubarea:number,
		public idPuesto: number,
		public idTipoPuesto: number,
		public idTipo: number
		){}
}



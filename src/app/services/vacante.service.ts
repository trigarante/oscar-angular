import  { Injectable } from '@angular/core';
import  { HttpClient, HttpHeaders } from '@angular/common/http';
import  { Observable } from 'rxjs/Observable';
import  { Vacante } from '../models/vacante';
import  { Global } from './global';
@Injectable ()
export class VacanteService {
	public url:string;
	constructor(
		private _http: HttpClient
		){

		this.url = Global.url;
		
	}

	testService(){
		return 'Probando el servicio de Angular';
	}

	saveVacante(vacante:Vacante): Observable<any>{
		
		let params = JSON.stringify(vacante);
		let headers = new HttpHeaders().set('Content-Type', 'application/json');
		return this._http.post(this.url+'saveVacante', params,{headers:headers});
	}

	getVacantes(): Observable<any>{

		let headers = new HttpHeaders().set('Content.Type','application/json');
		return this._http.get(this.url+'getALLVacantes',{headers:headers});


		

	}

	updateVacante(vacante): Observable <any> {
		let params = JSON.stringify(vacante);
		let headers = new HttpHeaders().set('Content.Type','application/json');
		return this._http.post(this.url+'saveVacante', params,{headers:headers});
	}


}

